package com.example.exchangerates.core.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "GiphyClient", url = "${giphy.url}")
public interface GiphyFeign {
    @GetMapping("/random")
    ResponseEntity<Map<String, Object>> getGif(@RequestParam("api_key") String apiKey,
                               @RequestParam("tag") String tag);
}
