package com.example.exchangerates.core.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "OpenExchangeRatesClient", url = "${openexchangerates.url}")
public interface OpenExchangeRatesFeign {
    @GetMapping("historical/{date}.json")
    String getExchangeRateForTheDate(@PathVariable String date,
                                            @RequestParam("app_id") String appId);
}
