package com.example.exchangerates.core.service.implementation;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface GiphyService {
    ResponseEntity<Map<String, Object>> getGifFromService(String tag);
}
