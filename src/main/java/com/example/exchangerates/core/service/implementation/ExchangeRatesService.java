package com.example.exchangerates.core.service.implementation;

public interface ExchangeRatesService {
    boolean courseComparison(String rate);
}
