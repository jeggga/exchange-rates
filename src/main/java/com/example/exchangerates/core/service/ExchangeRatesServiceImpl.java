package com.example.exchangerates.core.service;

import com.example.exchangerates.core.client.OpenExchangeRatesFeign;
import com.example.exchangerates.core.model.ExchangeRates;
import com.example.exchangerates.core.service.implementation.ExchangeRatesService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class ExchangeRatesServiceImpl implements ExchangeRatesService {
    @Autowired
    private OpenExchangeRatesFeign openExchangeRatesFeign;
    @Value("${openexchangerates.app_id}")
    private String appId;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String getTodayDate() {
        Date today = new Date();
        return dateFormat.format(today);
    }

    public String getYesterdayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date yesterdayDate = calendar.getTime();
        return dateFormat.format(yesterdayDate);
    }

    @Override
    public boolean courseComparison(String rate) {
        ExchangeRates exchangeRatesToday = new Gson().fromJson(openExchangeRatesFeign.getExchangeRateForTheDate(getTodayDate(), appId),
                ExchangeRates.class);
        ExchangeRates exchangeRatesYesterday = new Gson().fromJson(openExchangeRatesFeign.getExchangeRateForTheDate(getYesterdayDate(), appId),
                ExchangeRates.class);
        Double courseToday = exchangeRatesToday.getRates().get(rate);
        Double courseYesterday = exchangeRatesYesterday.getRates().get(rate);
        if (courseToday > courseYesterday) {
            return true;
        }
        return false;
    }
}
