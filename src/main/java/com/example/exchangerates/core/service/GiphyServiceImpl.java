package com.example.exchangerates.core.service;

import com.example.exchangerates.core.client.GiphyFeign;
import com.example.exchangerates.core.service.implementation.GiphyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class GiphyServiceImpl implements GiphyService {
    @Autowired
    private GiphyFeign giphyFeign;

    @Value("${giphy.api_key}")
    private String apiKey;
    @Override
    public ResponseEntity<Map<String, Object>> getGifFromService(String tag) {
        return giphyFeign.getGif(apiKey, tag);
    }
}
