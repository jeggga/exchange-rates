package com.example.exchangerates.core.controller;

import com.example.exchangerates.core.service.ExchangeRatesServiceImpl;
import com.example.exchangerates.core.service.GiphyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/exchangerates")
public class ExchangeRatesController {
    @Autowired
    private ExchangeRatesServiceImpl exchangeRatesService;
    @Autowired
    private GiphyServiceImpl giphyService;
    @Value("${giphy.rich}")
    private String tagRich;
    @Value("${giphy.broke}")
    private String tagBroke;

    @GetMapping("/gif/{rate}")
    public ResponseEntity<Map<String, Object>> getGif(@PathVariable String rate) {
        if (exchangeRatesService.courseComparison(rate)) {
            return giphyService.getGifFromService(tagRich);
        }
        return giphyService.getGifFromService(tagBroke);
    }
}
