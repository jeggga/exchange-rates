package com.example.exchangerates.core.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExchangeRatesServiceImplTest {
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    ExchangeRatesServiceImpl exchangeRatesService;


    @Test
    void getTodayDate() {
        Date today = new Date();
        String todayString = "2022-06-04"; // Необходимо ввести сегодняшнюю дата
        Assert.assertEquals(todayString, dateFormat.format(today));
    }

    @Test
    void getYesterdayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date yesterday = calendar.getTime();
        String yesterdayString = "2022-06-04"; // Необходимо ввести вчерашнюю дата
        Assert.assertEquals(yesterdayString, dateFormat.format(yesterday));
    }
}
