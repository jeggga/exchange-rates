package com.example.exchangerates.core.controller;

import com.example.exchangerates.core.service.ExchangeRatesServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(ExchangeRatesController.class)
public class ExchangeRatesControllerTest {
    @Value("${openexchangerates.app_id}")
    private String appId;
    @Value("${giphy.api_key}")
    private String apiKey;
    @Value("${giphy.rich}")
    private String tagRich;
    @Value("${giphy.broke}")
    private String tagBroke;

    @MockBean
    private MockMvc mockMvc;
    @MockBean
    ExchangeRatesServiceImpl exchangeRatesService;

    @Test
    public void returnRichGif() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("result", tagRich);
        ResponseEntity<Map<String, Object>> mapResponseEntity = new ResponseEntity<>(map, HttpStatus.OK);
        String rate = "RUB"; // любой код валюты
        Mockito.doReturn(mapResponseEntity);
        mockMvc.perform(
                        get("/exchangerates/gif/{rate}", rate).
                                contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.result").value(tagRich));
    }

    @Test
    public void returnBrokeGif() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("result", tagBroke);
        ResponseEntity<Map<String, Object>> mapResponseEntity = new ResponseEntity<>(map, HttpStatus.OK);
        String rate = "RUB"; // любой код валюты
        Mockito.doReturn(mapResponseEntity);
        mockMvc.perform(
                        get("/exchangerates/gif/{rate}", rate).
                                contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.result").value(tagBroke));
    }
}
