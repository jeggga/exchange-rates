# Exchange Rates
## Endpoints:
Get запрос на гифку
><http://localhost:8080/exchangerates/gif/RUB>

Вместо RUB может быть любой другой код валюты 

## Docker:
Сначала необходимо создать образ. Для этого следует перейти в корневую папку проекта и выполнить следующую команду:
>docker build -t springboot-docker-app:latest .

Для запуска приложения необходимо выполнить команду:
>docker run -p 8080:8080 springboot-docker-app


