# define base docker image
FROM openjdk:17
LABEL maintainer="ExchangeRates"
ADD build/libs/app.jar springboot-docker-app
ENTRYPOINT ["java", "-jar", "springboot-docker-app"]
